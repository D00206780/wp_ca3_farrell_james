<?php

  try {

    //Store config variables
    $config = require('config.php');
    define('APP_BASE_PATH', $config['app_base_path']);
  
    //Include the Rapid library
    require_once('lib/Rapid.php');

    //Create a new Router instance
    $app = new \Rapid\Router();

    //Define Regex param patterns
    $entity_id = '(?<entity_id>[0-9]{1,20})';
    $operation = '(?<operation>[a-zA-Z]{1,})';

    //Get Routes
    $app->GET('/',                                      'Home');

    //View Routes
    $app->GET("/view_customer",                         'ViewCustomer');
    $app->GET("/view_basket/$entity_id",                'ViewBasket');
    $app->GET("/view_product",                          'ViewProduct');
    $app->GET("/view_product/$entity_id",               'ViewProduct');

    //Delete Routes
    $app->GET("/delete_customer/$entity_id",            'DeleteCustomer'); 
    $app->GET("/delete_product/$entity_id",             'DeleteProduct'); 

    //Basket Routes
    $app->GET("/remove_product/$entity_id",             'RemoveProduct');
    $app->GET("/purchase_product/$entity_id",           'PurchaseProduct');

    //Create/Delete Routes
    $app->GET("/$operation" . "_customer",              'ViewCustomerForm');
    $app->GET("/$operation" . "_customer/$entity_id",   'ViewCustomerForm');
    $app->GET("/$operation" . "_product",               'ViewProductForm');
    $app->GET("/$operation" . "_product/$entity_id",    'ViewProductForm');

    //Post Routes
    $app->POST("/add_product/$entity_id",               'AddProduct');
    $app->POST("/$operation" . "_customer",             'ProcessCustomerForm');
    $app->POST("/$operation" . "_customer/$entity_id",  'ProcessCustomerForm');
    $app->POST("/$operation" . "_product",              'ProcessProductForm');
    $app->POST("/$operation" . "_product/$entity_id",   'ProcessProductForm');
    
    //Process the request
    $app->dispatch();

  } catch (\Rapid\RouteNotFoundException $e) {

    $e->getResponseObject()->render('main', 'error', [
      'error'         => '404 - Not Found!',
      'message'       => $e->getMessage(),
    ]);

    http_response_code(404); 
 
  } catch (ControllerNotFoundException $e) {

    $e->getResponseObject()->render('main', 'error', [
      'error'         => '404 - Not Found!',
      'message'       => $e->getMessage(),
    ]);

    http_response_code(404);

  } catch (RouteRedeclarationException $e) {

    $e->getResponseObject()->render('main', 'error', [
      'error'         => '409 - Conflict',
      'message'       => $e->getMessage(),
    ]);

    http_response_code(409);

  } catch (ViewNotFoundException $e) {

    $e->getResponseObject()->render('main', 'error', [
      'error'         => '404 - Not Found!',
      'message'       => $e->getMessage(),
    ]);

    http_response_code(404);

  } catch (LayoutNotFoundException $e) {

    $e->getResponseObject()->render('main', 'error', [
      'error'         => '404 - Not Found!',
      'message'       => $e->getMessage(),
    ]);

    http_response_code(404);

  } catch (Exception $e) {

    http_response_code(500);
    exit();

  }

?>