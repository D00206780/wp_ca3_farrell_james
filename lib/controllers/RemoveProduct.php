<?php return function($req, $res) {

    # Include dependencies
    $app_db_connection = include('lib/utils/Database.php');
    include('lib/models/Basket.php'); 
    include('lib/models/Product.php'); 

    # Retreive values from the route url
    $customer_id = $req->param('entity_id');
    $product_id  = $req->query('product_id');
    $quantity    = $req->query('quantity');

    # If the product id and quantity are set
    if ($product_id && $quantity) {

        # Add the quantity back to the products stock
        # Remove the proudct from the customer basket
        Product::increaseStock($app_db_connection, $quantity, $product_id);
        Basket::remove($app_db_connection, $customer_id, $product_id);

    } 

    # Redirect to the customer basket
    $res->redirect('/view_basket' . '/' . $customer_id);

} ?>