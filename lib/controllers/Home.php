<?php return function($req, $res) {
 
    # Render the home view
    $res->render('main', 'home', [
        'pageTitle'     => 'Home',
        'pageHeading'   => 'Home Page',
    ]);

} ?>