<?php return function($req, $res) {

    # Include dependencies
    $app_db_connection = include('lib/utils/Database.php');
    include('lib/models/Customer.php'); 

    # Retreive values from the database
    $customers = Customer::displayAll($app_db_connection) ?? [];

    # Render the view customer view
    $res->render('main', 'view_customer', [
        'pageTitle'         => 'Customer - WireMart',
        'pageHeading'       => 'Customer Page',
        'errors'            => [],
        'customers'         => $customers,
        'entity_id'         => NULL,
    ]);

} ?>