<?php return function($req, $res) {

    # Include dependencies
    $app_db_connection = include('lib/utils/Database.php');
    include('lib/models/Basket.php'); 
    include('lib/models/Product.php'); 

    # Retreive values from the route url
    $customer_id = $req->param('entity_id')  ?? NULL;
    $product_id  = $req->query('product_id') ?? NULL;
    $quantity    = $req->body('quantity')    ?? NULL;

    # Create empty errors array
    $errors = [];

    # If the product id has been set
    if ($product_id && $quantity) {

        # Check if there are enough of that item in stock
        if (Product::checkStock($app_db_connection, $quantity, $product_id)) {
  
            # Reduce product stock, add stock to the basket
            Product::reduceStock($app_db_connection, $quantity, $product_id);
            Basket::add($app_db_connection, $quantity, $customer_id, $product_id);
            $res->redirect('/view_basket' . '/' . $customer_id);
    
        } else {

            # Add error
            $errors = [
                'There doesn\'t seem to be enough ' . Product::getName($app_db_connection, $product_id) . ' left in stock.'
            ];

        }

    } else {

        # Add error
        $errors = [
            'There was an error adding that product to your basket',
        ];

    }

    # Retreive values from the database
    $products = Product::displayAll($app_db_connection, $customer_id);

    # Render the view product view
    $res->render('main', 'view_product', [
        'pageTitle'   => 'Product - WireMart',
        'pageHeading' => 'Product Page',
        'errors'      => $errors,
        'products'    => $products,
        'entity_id'   => $customer_id,
    ]);
    

} ?>