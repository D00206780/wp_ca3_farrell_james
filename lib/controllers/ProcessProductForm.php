<?php return function($req, $res) {

    # Include dependencies
    include('lib/errors/Errors.php');
    include('lib/models/Product.php');
    include('lib/utils/FormUtils.php');
    $app_db_connection = include('lib/utils/Database.php');

    # Store min/max values
    $min_price = $config['min_price'];
    $max_price = $config['max_price'];
    $min_stock = $config['min_stock'];
    $max_stock = $config['max_stock'];

    # Validate incoming post data
    $product_name        = FormUtils::getPostString($req->body('product_name'));
    $product_price       = FormUtils::getPostPrice($req->body('product_price'), $min_price, $max_price);
    $product_stock       = FormUtils::getPostInt($req->body('product_stock'), $min_stock, $max_stock);
    $product_code        = FormUtils::getPostProductCode($req->body('product_code'));
    $product_description = FormUtils::getPostString($req->body('product_description'));

    # Retreive values from the route url
    $operation   = $req->param('operation') ?? NULL;
    $product_id  = $req->param('entity_id') ?? NULL;
    $operationUc = ucwords($operation);

    # Check for existing values
    $product_code_exists = Product::checkIfExists($app_db_connection, 'product_code', $product_code['value']) ?? false;

    # Ignore existing value checks for update operations
    if ($operation == 'update') {
        $product_code_exists = false;
    }

    # Build an array of errors based on previous validatoin checks
    $errors = Errors::getProductFormErrorMessages($product_name, $product_price, $product_stock, $product_code, $product_description, $product_code_exists) ?? [];

    # Store product details in a product array
    $product = [
        'product_name'        => $product_name['value'],    
        'product_price'       => $product_price['value'],   
        'product_stock'       => $product_stock['value'],    
        'product_code'        => $product_code['value'],    
        'product_description' => $product_description['value'],
    ]; 

    # If there are no errors, and there is an operation to perform
    if (count($errors) == 0 && $operation) {
        
        # Perform the operation on the customer
        Product::$operationUc($app_db_connection, $product, $product_id);
        $res->redirect('view_product');

    } else {

        # Render the product form
        $res->render('main', 'product_form', [
            'pageTitle'   => 'Products - WireMart',
            'pageHeading' => 'Products Page',
            'errors'      => $errors,
            'operation'   => $operation,
            'entity_id'   => $product_id,
            'product'     => $product,
        ]);
        
    }

} ?>