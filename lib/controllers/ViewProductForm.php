<?php return function($req, $res) {

    # Include dependencies
    $app_db_connection = include('lib/utils/Database.php');
    include('lib/models/Product.php');

    # Retreive values from the route url
    $operation  = $req->param('operation') ?? NULL;
    $product_id = $req->param('entity_id') ?? NULL;

    # Redirect user if accessed via an unsupported route
    if ($operation != 'create' && $operation != 'update') {
        $res->redirect('/404');
    }

    # Retreive product details if update route requested.
    # These values will be used to populate the product form
    # with their existing details. 
    if ($operation == 'update' && $product_id) {
        $product = Product::displayProduct($app_db_connection, $product_id) ?? [];
    } else {
        $product = [];
    }

    # Render the product form
    $res->render('main', 'product_form', [
        'pageTitle'   => 'Products - WireMart',
        'pageHeading' => 'Products Page',
        'operation'   => $operation,
        'entity_id'   => $product_id,
        'product'     => $product,
    ]);

} ?>