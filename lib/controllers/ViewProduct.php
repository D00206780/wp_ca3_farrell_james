<?php return function($req, $res) {

    # Include dependencies
    $app_db_connection = include('lib/utils/Database.php');
    include('lib/models/Product.php'); 

    # Retreive the customer id (if accessed via basket)
    $customer_id = $req->param('entity_id') ?? NULL;

    # Retreive values from the database
    $products = Product::displayAll($app_db_connection) ?? [];

    # Render the view product view
    $res->render('main', 'view_product', [
        'pageTitle'         => 'Product - WireMart',
        'pageHeading'       => 'Product Page',
        'errors'            => [],
        'products'          => $products,
        'entity_id'         => $customer_id,
    ]);

} ?>