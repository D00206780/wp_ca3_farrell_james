<?php return function($req, $res) {

    # Include dependencies
    include('lib/errors/Errors.php');
    include('lib/models/Customer.php');
    include('lib/utils/FormUtils.php');
    $app_db_connection = include('lib/utils/Database.php');

    # Validate incoming post data
    $first_name     = FormUtils::getPostString($req->body('first_name'));
    $last_name      = FormUtils::getPostString($req->body('last_name'));
    $gender         = FormUtils::getPostGender($req->body('gender'));
    $phone_number   = FormUtils::getPostNumber($req->body('phone_number'));
    $email_address  = FormUtils::getPostEmail($req->body('email_address'));
    $eir_code       = FormUtils::getPostEirCode($req->body('eir_code'));
    $address_line_1 = FormUtils::getPostString($req->body('address_line_1'));
    $address_line_2 = FormUtils::getPostString($req->body('address_line_2'));
    $town_city      = FormUtils::getPostString($req->body('town_city'));
    $county         = FormUtils::getPostString($req->body('county'));
    $country        = FormUtils::getPostString($req->body('country'));

    # Retreive values from the route url
    $operation   = $req->param('operation') ?? NULL;
    $customer_id = $req->param('entity_id') ?? NULL;
    $operationUc = ucwords($operation);

    # Check for existing values
    $phone_exists   = Customer::checkIfExists($app_db_connection, 'phone_number', $phone_number['value'])   ?? false;
    $email_exists   = Customer::checkIfExists($app_db_connection, 'email_address', $email_address['value']) ?? false;

    # Ignore existing value checks for update operations
    if ($operation == 'update') {
        $phone_exists = false;
        $email_exists = false;
    }

    # Build an array of errors based on previous validation checks
    $errors = Errors::getCustomerFormErrorMessages($first_name, $last_name, $gender, $phone_number, $email_address, $eir_code, $address_line_1, $address_line_2, $town_city, $county, $country, $phone_exists, $email_exists);    
    
    # Store customer details in a customer array
    $customer = [
        'first_name'        => $first_name['value'],
        'last_name'         => $last_name['value'],
        'gender'            => $gender['value'],
        'phone_number'      => $phone_number['value'],
        'email_address'     => $email_address['value'],
        'eir_code'          => $eir_code['value'],
        'address_line_1'    => $address_line_1['value'],
        'address_line_2'    => $address_line_2['value'],
        'town_city'         => $town_city['value'],
        'county'            => $county['value'],
        'country'           => $country['value']
    ]; 

    # If there are no errors, and there is an operation to perform
    if (count($errors) == 0 && $operation) {

        # Perform the operation on the customer
        Customer::$operationUc($app_db_connection, $customer, $customer_id);
        $res->redirect('view_customer');

    } else {

        # Render the customer form
        $res->render('main', 'customer_form', [
            'pageTitle'   => 'Customers - WireMart',
            'pageHeading' => 'Customers Page',
            'errors'      => $errors,
            'operation'   => $operation,
            'entity_id'   => $customer_id,
            'customer'    => $customer,
        ]);

    }

} ?>