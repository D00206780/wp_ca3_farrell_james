<?php return function($req, $res) {

    # Include dependencies
    $app_db_connection = include('lib/utils/Database.php');
    include('lib/models/Basket.php'); 

    # Retreive values from the route url
    $customer_id = $req->param('entity_id');
    $product_id  = $req->query('product_id');

    # If the cproduct id is set
    if ($product_id) {

        # Remove the product from the customer basket
        # Return 'purchase sucessful' message to the user
        Basket::remove($app_db_connection, $customer_id, $product_id);
        $res->redirect('/view_basket' . '/' . $customer_id . '?msg=1');

    } 

    # Redirect to the customer basket
    $res->redirect('/view_basket' . '/' . $customer_id);

} ?>