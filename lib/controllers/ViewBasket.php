<?php return function($req, $res) {

    # Include dependencies
    $app_db_connection = include('lib/utils/Database.php');
    include('lib/models/Basket.php'); 

    # Retreive values from the route url
    $customer_id = $req->param('entity_id') ?? NULL;
    $message     = $req->query('msg')       ?? NULL;

    # Retreive the items in the customers basket
    $basket_items = Basket::displayAll($app_db_connection, $customer_id) ?? [];

    # Render the view basket view
    $res->render('main', 'view_basket', [
        'pageTitle'         => 'Basket - WireMart',
        'pageHeading'       => 'Basket Page',
        'basket_items'      => $basket_items,
        'message'           => $message,
        'entity_id'         => $customer_id,
    ]);    
    
} ?>