<?php return function($req, $res) {

    # Include Dependencies
    $app_db_connection = include('lib/utils/Database.php');
    include('lib/models/Product.php'); 

    # Retreive values from the route url
    $product_id = $req->param('entity_id') ?? NULL;

    # Delete the given product 
    Product::delete($app_db_connection, $product_id);
    $res->redirect('view_product');

} ?>