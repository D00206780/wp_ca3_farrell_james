<?php return function($req, $res) {

    # Include Dependencies
    $app_db_connection = include('lib/utils/Database.php');
    include('lib/models/Customer.php'); 

    # Retreive values from the route url
    $customer_id = $req->param('entity_id') ?? NULL;

    # Delete the given customer
    Customer::delete($app_db_connection, $customer_id);
    $res->redirect('view_customer');
    
} ?>