<?php return function($req, $res) {

    # Include dependencies
    $app_db_connection = include('lib/utils/Database.php');
    include('lib/models/Customer.php');

    # Retreive values from the route url
    $operation   = $req->param('operation') ??   '';
    $customer_id = $req->param('entity_id') ?? NULL;

    # Redirect user if accessed via an unsupported route
    if ($operation != 'create' && $operation != 'update') {
        $res->redirect('/404');
    }

    # Retreive customer details if update route requested.
    # These values will be used to populate the customer form
    # with their existing details. 
    if ($operation == 'update' && $customer_id) {
        $customer = Customer::displayCustomer($app_db_connection, $customer_id) ?? [];
    } else {
        $customer = [];
    }

    # Render the customer form
    $res->render('main', 'customer_form', [
        'pageTitle'   => 'Customers - WireMart',
        'pageHeading' => 'Customers Page',
        'errors'      => [],
        'operation'   => $operation,
        'entity_id'   => $customer_id,
        'customer'    => $customer,
    ]);

} ?>