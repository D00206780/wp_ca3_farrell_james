<?php 

  class FormUtils {

    /**
     * $index: the value to validate and sanitize
     * $sanitize_func: sanitization filter to apply (or NULL for none)
     * $validate_func: validation filter to apply (or NULL for none)
     * 
     */
    public static function getPostValue($index, $sanitize_func, $validate_func) {

      # Basic check: was a non-empty value posted
      $was_posted = $index ?? NULL;
      if (!$was_posted) {
        return [
          'value'    => '',
          'is_valid' => false
        ];
      }

      # Now sanitize (if requested)
      $value = $index;

      if ($sanitize_func !== NULL) {
        $value = filter_var($value, $sanitize_func);
      }

      # Now validate (if requested)
      $is_valid = true;
      if ($validate_func !== NULL) {
        $is_valid = filter_var($value, $validate_func);
      }

      # Return sanitized value, and boolean to
      # indicate valid or not.
      return [
        'value'    => $value,
        'is_valid' => $is_valid
      ];
    }

    /**
     * Get a required post integer in an allowed range
     */
    public static function getPostInt($index, $min = PHP_INT_MIN, $max = PHP_INT_MAX) {

      # Use the generic method to get the raw value
      $raw_value = FormUtils::getPostValue($index, FILTER_SANITIZE_NUMBER_INT, FILTER_VALIDATE_INT);

      # If the generic check was valid, apply further checks
      if ($raw_value['is_valid']) {

        # The value is still a string (all post values start as string)
        # Since we're asking for a number, we should convert it to
        # a number type (by type casting)
        $raw_value['value'] = (int)$raw_value['value'];

        # Check the int is in expected ranges
        if ($raw_value['value'] < $min || $raw_value['value'] > $max) {
          $raw_value['is_valid'] = false;
        }

      }

      return $raw_value;
    }

    /**
     * Get a posted string, which can be empty or not
     */
    public static function getPostString($index, $allow_empty = false) {
      $raw_value = FormUtils::getPostValue($index, FILTER_SANITIZE_STRING, NULL);

      if ($raw_value['is_valid']) {

        # Trim white space
        $raw_value['is_valid'] = trim($raw_value['value']);

        if (!$allow_empty && empty($raw_value['value'])) {
          $raw_value['is_valid'] = false;
        }

      }

      return $raw_value;
    }

    /**
     * No specialization here, we are simply wrapping the
     * generic method in another method with a more meaningful
     * name.
     */
    public static function getPostEmail($index) {
      return FormUtils::getPostValue($index, FILTER_SANITIZE_EMAIL,  FILTER_VALIDATE_EMAIL);
    }

    public static function getPostGender($index) {
      $raw_value = FormUtils::getPostValue($index, FILTER_SANITIZE_STRING, NULL);

      if ($raw_value['is_valid']) {

        # Trim white space
        $raw_value['value'] = trim($raw_value['value']);

        # Check if the gender entered is acceptable
        if ($raw_value['value'] != 'M' && $raw_value['value'] != 'F' && $raw_value['value'] != 'O') {
          $raw_value['is_valid'] = false;
        }

      }

      return $raw_value;
    }

    public static function getPostNumber($index) {
      $raw_value = FormUtils::getPostValue($index, FILTER_SANITIZE_STRING, NULL);

      if ($raw_value['is_valid']) {

        # Trim white space
        $raw_value['value'] = trim($raw_value['value']);

        # Check if the phone number matches required format
        if (!preg_match('/^08[0-9]{1}-{1}[0-9]{3}-[0-9]{4}$/', $raw_value['value'])) {
          $raw_value['is_valid'] = false;
        }   

      }

      return $raw_value;
    }

    public static function getPostEirCode($index) {
      $raw_value = FormUtils::getPostValue($index, FILTER_SANITIZE_STRING, NULL);

      if ($raw_value['is_valid']) {

        # Trim white space
        $raw_value['value'] = trim($raw_value['value']);

        # Check if the EirCode matches the required format
        if (!preg_match('/^([a-zA-Z]{1}[0-9]{2}|[Dd]{1}[6]{1}[Ww]{1})\s{1}[a-zA-z0-9]{4}$/', $raw_value['value'])) {
          $raw_value['is_valid'] = false;
        }   

      }

      return $raw_value;
    }

    public static function getPostProductCode($index) {
      $raw_value = FormUtils::getPostValue($index, FILTER_SANITIZE_STRING, NULL);

      if ($raw_value['is_valid']) {

        # Trim white space
        $raw_value['value'] = trim($raw_value['value']);

        # Check if the product code matches the required format
        if (!preg_match('/^[a-zA-Z]{3}-{1}[0-9]{4}$/', $raw_value['value'])) {
          $raw_value['is_valid'] = false;
        }   

      }

      return $raw_value;
    }

    public static function getPostPrice($index, $min, $max) {

      //Snippet taken from Stack Overflow: https://stackoverflow.com/questions/6066809/why-does-the-filter-validate-number-float-constant-drop-the-decimal-place-char
      $sanitize = filter_input(
        FILTER_SANITIZE_NUMBER_FLOAT, 
        FILTER_FLAG_ALLOW_FRACTION
      );

      # Use the generic method to get the raw value
      $raw_value = FormUtils::getPostValue($index, $sanitize, FILTER_VALIDATE_FLOAT);

      # If the generic check was valid, apply further checks
      if ($raw_value['is_valid']) {

        # Format the value to two deicmal places
        $raw_value['value'] = number_format($raw_value['value'], 2);

        # Check that the price is in expected ranges
        if ($raw_value['value'] < $min || $raw_value['value'] > $max) {
          $raw_value['is_valid'] = false;
        }

      }

      return $raw_value;
    }

  }

?>