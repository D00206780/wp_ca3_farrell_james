<?php 
    try {

        $config = require('config.php');

        $app_db_connection = new PDO(
            'mysql:host=' . $config['db_host'] . ';dbname=' . $config['db_name'] . ';charset=utf8mb4',
            $config['db_user'],
            $config['db_pass']
        );

        return $app_db_connection;

    } catch (PDOException $e) {

        $res->render('main', 'error', [
            'error'         => $e->getCode() . ' - PDO Exception',
            'message'       => $e->getMessage(),
        ]);

        http_response_code($e->getCode()); 

        exit();
        
    }
?>