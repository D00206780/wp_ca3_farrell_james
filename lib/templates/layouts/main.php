<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title><?= $locals['pageTitle'] ?? 'WireMart' ?></title>
        <link rel='stylesheet' href='<?= APP_BASE_PATH ?>/assets/styles/site.css'>
    </head>
    <body>
        <h1><?= $locals['pageHeading'] ?? 'WireMart' ?></h1>

        <nav>
            <ul>
                <li><a href='<?= APP_BASE_PATH ?>/'>Home</a></li>
            </ul>
        </nav>

        <?= \Rapid\Renderer::VIEW_PLACEHOLDER ?>
    </body>
</html>