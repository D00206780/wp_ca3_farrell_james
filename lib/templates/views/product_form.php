<!-- Display a list of errors to the user -->
<?php if ($locals['errors']) { ?>
    <h3>Errors:</h3>
    <ul>
        <?php foreach ($locals['errors'] as $error) { ?>
            <li><?= $error ?></li>
        <?php } ?>
    </ul>
<?php } ?>

<!-- Display information about the current operation being performed -->
<h3><?= ucwords($locals['operation']) ?? '' ?> Product</h3>

<!-- Create or update a product -->
<form action='<?= APP_BASE_PATH ?>/<?= $locals['operation'] ?>_product/<?= $locals['entity_id'] ?>' method='post' class='table_form'>
    <div>
        <label for='product_name'>Name:</label>
        <input type='text' id='product_name' name='product_name' value='<?= $locals['product']['product_name'] ?? '' ?>' maxlength='100' required autofocus>
    </div>
    
    <div>
        <!-- Regex snippet taken from: https://stackoverflow.com/questions/3517468/php-regular-expression-to-match-price-or-amount -->
        <!-- Regex updated to support .XX and .X -->
        <label for='product_price'>Price:</label>
        &euro; <input type='text' id='product_price' name='product_price' value='<?= $locals['product']['product_price'] ?? '' ?>' pattern='^([1-9][0-9]*|0{0,1})(\.[0-9]{1,2})?$' title='Max of 2 decimal places' required>
    </div>
    
    <div>
        <label for='product_stock'>Stock:</label>
        <input type='number' id='product_stock' name='product_stock' value='<?= $locals['product']['product_stock'] ?? '' ?>' min='1' required>
    </div>
    
    <div>
        <label for='product_code'>Code:</label>
        <input type='text' id='product_code' name='product_code' value='<?= $locals['product']['product_code'] ?? '' ?>' pattern='^[a-zA-Z]{3}-{1}[0-9]{4}$' title='ABC-3456' required>
    </div>
    
    <div>
        <label for='product_description'>Description:</label>
        <input type='text' id='product_description' name='product_description' value='<?= $locals['product']['product_description'] ?? '' ?>' maxlength='255' required>
    </div>

    <div>
        <input type='submit' value='Submit!'>
    </div>
</form>