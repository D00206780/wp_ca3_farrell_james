<!-- Display a list of errors to the user -->
<?php if ($locals['errors']) { ?>
    <h3>Errors:</h3>
    <ul>
        <?php foreach ($locals['errors'] as $error) { ?>
            <li><?= $error ?></li>
        <?php } ?>
    </ul>
<?php } ?>

<!-- Display information about the current operation being performed -->
<h3><?= ucwords($locals['operation']) ?? '' ?> Customer</h3>

<!-- Create or update a customer -->
<form action='<?= APP_BASE_PATH ?>/<?= $locals['operation'] ?>_customer/<?= $locals['entity_id'] ?? '' ?>' method='post'>
    <div>
        <label for='first_name'>Forename:</label>
        <input type='text' id='first_name' name='first_name' value='<?= $locals['customer']['first_name'] ?? '' ?>' maxlength='50' required autofocus>
    </div>
    
    <div>
        <label for='last_name'>Surname:</label>
        <input type='text' id='last_name' name='last_name' value='<?= $locals['customer']['last_name'] ?? '' ?>' maxlength='50' required>
    </div>
    
    <!-- Support must be added here for auto-filing with data stored in the database -->
    <div>
        <label for='gender'>Gender:</label>
        <select name='gender' id='gender'>
            <option value=''>Please Select</option>
            <option value='M'>Male</option>
            <option value='F'>Female</option>
            <option value='O'>Other</option>
        </select>
    </div>
    
    <div>
        <label for='phone_number'>Phone:</label>
        <input type='text' id='phone_number' name='phone_number' value='<?= $locals['customer']['phone_number'] ?? '' ?>' maxlength='20' pattern='^08[0-9]{1}-{1}[0-9]{3}-[0-9]{4}$' title='08X-XXX-XXXX' required>
    </div>
    
    <div>
        <label for='email_address'>Email:</label>
        <input type='email' id='email_address' name='email_address' value='<?= $locals['customer']['email_address'] ?? '' ?>' maxlength='150' required>
    </div>
    
    <div>
        <label for='eir_code'>Eir Code:</label>
        <input type='text' id='eir_code' name='eir_code' value='<?= $locals['customer']['eir_code'] ?? '' ?>' maxlength='16' pattern='^([a-zA-Z]{1}[0-9]{2}|[Dd]{1}[6]{1}[Ww]{1})\s{1}[a-zA-z0-9]{4}$' title='Please enter a valid EirCode.'>
    </div>

    <div>
        <label for='address_line_1'>Address Line 1:</label>
        <input type='text' id='address_line_1' name='address_line_1' value='<?= $locals['customer']['address_line_1'] ?? '' ?>' maxlength='100' required>
    </div>

    <div>
        <label for='address_line_2'>Address Line 2:</label>
        <input type='text' id='address_line_2' name='address_line_2' value='<?= $locals['customer']['address_line_2'] ?? '' ?>' maxlength='100' required>
    </div>

    <div>
        <label for='town_city'>Town/City:</label>
        <input type='text' id='town_city' name='town_city' value='<?= $locals['customer']['town_city'] ?? '' ?>' maxlength='60' required>
    </div>
    
    <!-- Support must be added here for auto-filing with data stored in the database -->
    <div>
    	<label for='county'>County:</label>
    	<select name='county' id='county' required>
            <option value=''>Please Select</option>
            <option value='Carlow'>Carlow</option>
            <option value='Cavan'>Cavan</option>
            <option value='Clare'>Clare</option>
            <option value='Cork'>Cork</option>
            <option value='Donegal'>Donegal</option>
            <option value='Dublin'>Dublin</option>
            <option value='Galway'>Galway</option>
            <option value='Kerry'>Kerry</option>
            <option value='Kildare'>Kildare</option>
            <option value='Kilkenny'>Kilkenny</option>
            <option value='Laois'>Laois</option>
            <option value='Leitrim'>Leitrim</option>
            <option value='Limerick'>Limerick</option>
            <option value='Longford'>Longford</option>
            <option value='Louth'>Louth</option>
            <option value='Mayo'>Mayo</option>
            <option value='Meath'>Meath</option>
            <option value='Monaghan'>Monaghan</option>
            <option value='Offaly'>Offaly</option>
            <option value='Roscommon'>Roscommon</option>
            <option value='Sligo'>Sligo</option>
            <option value='Tipperary'>Tipperary</option>
            <option value='Waterford'>Waterford</option>
            <option value='Westmeath'>Westmeath</option>
            <option value='Wexford'>Wexford</option>
            <option value='Wicklow'>Wicklow</option>
        </select>
    </div>

    <div>
        <label for='country'>Country:</label>
        <select name='country' id='country' value='<?= $locals['customer']['country'] ?? '' ?>' required>
            <option value='Ireland'>Ireland</option>
        </select>
    </div>    

    <div>
        <input type='submit' value='Submit!'>
    </div>
</form>