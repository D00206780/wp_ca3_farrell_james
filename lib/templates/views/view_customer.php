<h3>View customers</h3>

<!-- List of menu options -->
<nav>
    <ul>
        <li><a href='<?= APP_BASE_PATH ?>/create_customer'>Create Customer</a></li>
    </ul>
</nav>

<!-- Display a list of errors to the user -->
<?php if ($locals['errors']) { ?>
    <h3>Errors:</h3>
    <ul>
        <?php foreach ($locals['errors'] as $error) { ?>
            <li><?= $error ?></li>
        <?php } ?>
    </ul>
<?php } ?>

<!-- If there are customers in the array  -->
<?php if ($locals['customers']) { ?>
    <table>
        <tr>
            <th>Forname</th>
            <th>Surname</th>
            <th>Gender</th>
            <th>Phone Number</th>
            <th>Email Address</th>
            <th>Eir Code</th>
            <th>Address Line 1</th>
            <th>Address Line 2</th>
            <th>Town/City</th>
            <th>County</th>
            <th>Country</th>
        </tr>

        <!-- Loop through each customer in the array -->
        <?php foreach ($locals['customers'] as $customer) { ?>        
            <tr>
                <td><?= $customer->get('first_name') ?></td>
                <td><?= $customer->get('last_name') ?></td>
                <td><?= $customer->get('gender') ?></td>
                <td><?= $customer->get('phone_number') ?></td>
                <td><?= $customer->get('email_address') ?></td>
                <td><?= $customer->get('eir_code') ?></td>
                <td><?= $customer->get('address_line_1') ?></td>
                <td><?= $customer->get('address_line_2') ?></td>
                <td><?= $customer->get('town_city') ?></td>
                <td><?= $customer->get('county') ?></td>
                <td><?= $customer->get('country') ?></td>
                <td><a href='<?= APP_BASE_PATH ?>/update_customer/<?= $customer->get('customer_id') ?>'><button>Edit</button></a></td>
                <td><a href='<?= APP_BASE_PATH ?>/delete_customer/<?= $customer->get('customer_id') ?>'><button>Delete</button></a></td>
                <td><a href='<?= APP_BASE_PATH ?>/view_basket/<?= $customer->get('customer_id') ?>'><button>View Basket</button></a></td>
            </tr>
        <?php } ?>
    </table>

<!-- Else prompt the user to create a customer -->
<?php } else { ?>
        <h3>No customers yet! <a href='<?= APP_BASE_PATH ?>/create_customer'>Create One</a><h3>
<?php } ?>