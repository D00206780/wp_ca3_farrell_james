<h3>View products</h3>

<!-- Display a view basket option if the customer id is set -->
<?php if ($locals['entity_id']) { ?> 
    <nav>
        <ul>
            <li><a href='<?= APP_BASE_PATH ?>/view_basket/<?= $locals['entity_id'] ?>'>View Basket</a></li>
        </ul>
    </nav>

<!-- Display a create product option if the customer id is not set -->
<?php } else { ?>
    <nav>
        <ul>
            <li><a href='<?= APP_BASE_PATH ?>/create_product'>Create Product</a></li>
        </ul>
    </nav>
<?php } ?>

<!-- Display a list of errors to the user -->
<?php if ($locals['errors']) { ?>
    <h3>Errors:</h3>
    <ul>
        <?php foreach ($locals['errors'] as $error) { ?>
            <li><?= $error ?></li>
        <?php } ?>
    </ul>
<?php } ?>

<!-- If there are products in the array  -->
<?php if ($locals['products']) { ?>
    <table>
        <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Stock</th>
            <th>Code</th>
            <th>Description</th>
        </tr>

        <!-- Loop through each product in the locals array -->
        <?php foreach ($locals['products'] as $product) { ?>        
            <tr>
                <td><?= $product->get('product_name') ?></td>
                <td><?= $product->get('product_price') ?></td>
                <td><?= $product->get('product_stock') ?></td>
                <td><?= $product->get('product_code') ?></td>
                <td><?= $product->get('product_description') ?></td>

                <!-- Don't display these options if a customer id is set -->
                <?php if (!$locals['entity_id']) { ?>
                    <td><a href='<?= APP_BASE_PATH ?>/update_product/<?= $product->get('product_id') ?>'><button>Edit</button></a></td>
                    <td><a href='<?= APP_BASE_PATH ?>/delete_product/<?= $product->get('product_id') ?>'><button>Delete</button></a></td>
                <?php } ?>

                <!-- Display add to basket form if a customer id is set, and the products stock is greater than 0 -->
                <?php if ($locals['entity_id'] && $product->get('product_stock') > 0) { ?>
                    <td>

                        <!-- Add x amount of this product to the customers basket -->
                        <form action='<?= APP_BASE_PATH ?>/add_product/<?= $locals['entity_id'] ?>?product_id=<?= $product->get('product_id') ?>' method='post'>
                            <div>
                                <input type='number' id='quantity' name='quantity' min='1' max='<?= $product->get('product_stock') ?>' required>
                                <input type='submit' value='Add to Basket'>
                            </div>
                        </form>
                    </td>

                <!-- Display out of stock message if a customer id is set, but the products stock is <= 0 -->
                <?php } else if ($locals['entity_id'] && $product->get('product_stock') <= 0) { ?>
                    <td>Out of Stock!</td>
                <?php } ?>
            </tr>
        <?php } ?>
    </table>

<!-- Else prompt the user to create a product -->
<?php } else { ?>
        <h3>No products yet! <a href='<?= APP_BASE_PATH ?>/create_product'>Add Some</a><h3>
<?php } ?>