<h3>Basket</h3>

<!-- List of menu options -->
<nav>
    <ul>
        <li><a href='<?= APP_BASE_PATH ?>/view_customer/'>View Customers</a></li>
        <li><a href='<?= APP_BASE_PATH ?>/view_product/<?= $locals['entity_id'] ?>'>Browse Products</a></li>
    </ul>
</nav>

<!-- Display messages -->
<?php if ($locals['message'] == 1) { ?>
    <h3>Purchase Sucessful!</h3>
<?php } ?>

<!-- If there are items in the basket -->
<?php if ($locals['basket_items']) { ?>
    <table>
        <tr>
            <th>Product Name</th>
            <th>Product Price</th>
            <th>Quantity</th>
            <th>Total Cost</th>
        </tr>

        <!-- Loop through each basket item in the array -->
        <?php foreach ($locals['basket_items'] as $basket_item) { ?>        
            <tr>
                <td><?= $basket_item->get('product_name') ?></td>
                <td><?= $basket_item->get('product_price') ?></td>
                <td><?= $basket_item->get('quantity') ?></td>
                <td><?= $basket_item->get('total_cost') ?></td>
                <td><a href='<?= APP_BASE_PATH ?>/purchase_product/<?= $basket_item->get('customer_id') ?>?product_id=<?= $basket_item->get('product_id') ?>'><button>Purchase</button></a></td>
                <td><a href='<?= APP_BASE_PATH ?>/remove_product/<?=   $basket_item->get('customer_id') ?>?product_id=<?= $basket_item->get('product_id') ?>&quantity=<?= $basket_item->get('quantity') ?>'><button>Delete</button></a></td>
            </tr>
        <?php } ?>
    </table>

<!-- If the user has not just bought an item, and there are no items in their basket -->
<?php } else if ($locals['message'] != 1) { ?>
        <h3>No items in basket! <a href='<?= APP_BASE_PATH ?>/view_product/<?= $locals['entity_id'] ?>'>Find items</a><h3>
<?php } ?>

