<?php 

    class Errors {
        
        # Display errors based on customer form input
        public static function getCustomerFormErrorMessages($first_name, $last_name, $gender, $phone_number, $email_address, $eir_code, $address_line_1, $address_line_2, $town, $county, $country, $phone_exists, $email_exists) {

            $form_error_messages = [];
      
            if (!$first_name['is_valid']) {
              $form_error_messages['first_name'] = 'Please enter a firstname.';
            }
      
            if (!$last_name['is_valid']) {
              $form_error_messages['last_name'] = 'Please enter a lastname.';
            }

            if (!$gender['is_valid']) {
                $form_error_messages['gender'] = 'Please enter a valid gender!';
            }

            if (!$phone_number['is_valid']) {
                $form_error_messages['phone_number'] = 'Please enter a valid phone number.';
            }

            if (!$email_address['is_valid']) {
                $form_error_messages['email_address'] = 'Please enter a valid email address.';
            }

            if (!$eir_code['is_valid']) {
                $form_error_messages['eir_code'] = 'Please enter a valid eir code.';
            }

            if (!$address_line_1['is_valid']) {
                $form_error_messages['address_line_1'] = 'Please enter a valid address line.';
            }

            if (!$address_line_2['is_valid']) {
                $form_error_messages['address_line_2'] = 'Please enter a valid address line.';
            }
      
            if (!$town['is_valid']) {
                $form_error_messages['town'] = 'Please enter a town.';
            }

            if (!$county['is_valid']) {
                $form_error_messages['county'] = 'Please enter a county.';
            }

            if (!$country['is_valid']) {
                $form_error_messages['country'] = 'Please enter a country.';
            }

            if ($phone_exists) {
                $form_error_messages['phone_exists'] = 'Phone number already exists!';
            }

            if ($email_exists) {
                $form_error_messages['email_exists'] = 'Email address already exists!';
            }

            return $form_error_messages;
      
        }

        # Display errors based on product form input
        public static function getProductFormErrorMessages($product_name, $product_price, $product_stock, $product_code, $product_description, $product_code_exists) {

            $form_error_messages = [];
      
            if (!$product_name['is_valid']) {
              $form_error_messages['product_name'] = 'Please enter a product name.';
            }
      
            if (!$product_price['is_valid']) {
              $form_error_messages['product_price'] = 'Please enter a valid price.';
            }

            if (!$product_stock['is_valid']) {
                $form_error_messages['product_stock'] = 'Please enter a valid number.';
            }

            if (!$product_code['is_valid']) {
                $form_error_messages['product_code'] = 'Please enter a valid product code.';
            }

            if (!$product_description['is_valid']) {
                $form_error_messages['product_description'] = 'Please enter a valid description.';
            }

            if ($product_code_exists) {
                $form_error_messages['product_code_exists'] = 'That product code already exists!';
            }

            return $form_error_messages;
      
        }

    }

?>