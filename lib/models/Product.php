<?php

    class Product {

        private $product_id;
        private $product_name;
        private $product_price;
        private $product_stock;
        private $product_code;
        private $product_description;

        public function __construct($product_id, $product_name, $product_price, $product_stock, $product_code, $product_description) {
            $this->product_id          = $product_id;
            $this->product_name        = $product_name;
            $this->product_price       = $product_price;
            $this->product_stock       = $product_stock;
            $this->product_code        = $product_code;
            $this->product_description = $product_description;
        }

        # Retreive data about this product
        # @param member variable name
        # return member variable value
        public function get($column_name) {
            return $this->$column_name;
        }

        # Retreive the name of a given product
        # @param the products id
        # return product name
        public static function getName($app_db_connection, $product_id) {
            $select = $app_db_connection->prepare('SELECT product_name FROM product WHERE product_id = :product_id');
            $select->bindParam(':product_id', $product_id);
            $select->execute();
            $select = $select->fetch();
            return $select['product_name'];
        }

        # Create a new row in the product table
        # @param database PDO
        # @param array of product details
        public static function create($app_db_connection, $product) {
            $insert = $app_db_connection->prepare('INSERT INTO product (product_name, product_price, product_stock, product_code, product_description) VALUES (:product_name, :product_price, :product_stock, :product_code, :product_description);');
            $insert->bindParam(':product_name',             $product['product_name']);
            $insert->bindParam(':product_price',            $product['product_price']);
            $insert->bindParam(':product_stock',            $product['product_stock']);
            $insert->bindParam(':product_code',             $product['product_code']);
            $insert->bindParam(':product_description',      $product['product_description']);
            $insert->execute();
        }

        # Update a row in the products table
        # @param database PDO
        # @param array of product details
        # @param the products id
        public static function update($app_db_connection, $product, $product_id) {
            $update = $app_db_connection->prepare('UPDATE product SET product_name = :product_name, product_price = :product_price, product_stock = :product_stock, product_code = :product_code, product_description = :product_description WHERE product_id = :product_id;');
            $update->bindParam(':product_id',               $product_id);
            $update->bindParam(':product_name',             $product['product_name']);
            $update->bindParam(':product_price',            $product['product_price']);
            $update->bindParam(':product_stock',            $product['product_stock']);
            $update->bindParam(':product_code',             $product['product_code']);
            $update->bindParam(':product_description',      $product['product_description']);
            $update->execute(); 
        }

        # Delete a row from the product table
        # @param database PDO
        # @param the products id
        public static function delete($app_db_connection, $product_id) {
            $delete = $app_db_connection->prepare('DELETE FROM product WHERE product_id = :product_id');
            $delete->bindParam(':product_id',               $product_id);
            $delete->execute();
        }

        # Display details about a single product
        # @param database PDO
        # @param the products id
        # return array of product details
        public static function displayProduct($app_db_connection, $product_id) {
            $select = $app_db_connection->prepare('SELECT * FROM product WHERE product_id = :product_id');
            $select->bindParam(':product_id',               $product_id);
            $select->execute();
            $product = $select->fetch();

            $product = new Product($product['product_id'], $product['product_name'], $product['product_price'], $product['product_stock'], $product['product_code'], $product['product_description']);
            
            return [
                'product_id'          => $product->get('product_id')          ?? '',
                'product_name'        => $product->get('product_name')        ?? '',
                'product_price'       => $product->get('product_price')       ?? '',
                'product_stock'       => $product->get('product_stock')       ?? '',
                'product_code'        => $product->get('product_code')        ?? '',
                'product_description' => $product->get('product_description') ?? '',
            ];
        }

        # Display all products in table
        # @param database PDO
        # retrun array of product objects
        public static function displayAll($app_db_connection) {
            $select = $app_db_connection->query('SELECT * FROM product');
            $items  = $select->fetchAll();

            $products = [];

            foreach ($items as $item) {
                $product = new Product($item['product_id'], $item['product_name'], $item['product_price'], $item['product_stock'], $item['product_code'], $item['product_description']);
                array_push($products, $product);
            }

            return $products;
        }

        # Check if a value exists in the table
        # @param database PDO
        # @param name of column to check
        # @param name of value to check
        # return true if value exists
        public static function checkIfExists($app_db_connection, $column_name, $column_value) {
            $select = $app_db_connection->prepare("SELECT * FROM product WHERE $column_name = :column_value");
            $select->bindParam(':column_value',             $column_value);
            $select->execute();

            return $select->rowCount() !== 0;
        }

        # Check if there is sufficient stock to complete an order
        # @param database PDO
        # @param order quantity
        # @param products id
        # return true if there is sufficient stock
        public static function checkStock($app_db_connection, $quantity, $product_id) {
            $select = $app_db_connection->prepare('SELECT product_id FROM product WHERE product_stock - :quantity >= 0 AND product_id = :product_id LIMIT 1');
            $select->bindParam(':quantity',                 $quantity);
            $select->bindParam(':product_id',               $product_id);
            $select->execute();

            return $select->rowCount() !== 0;
        }

        # Increase the quantity of a products stock
        # @param database PDO
        # @param quantity to raise stock by
        # @param the products id
        public static function increaseStock($app_db_connection, $quantity, $product_id) {
            $update = $app_db_connection->prepare('UPDATE product SET product_stock = product_stock + :quantity WHERE product_id = :product_id');
            $update->bindParam(':quantity',                 $quantity);
            $update->bindParam(':product_id',               $product_id);
            $update->execute();
        }

        # Reduce the quantity of a products stock
        # @param database PDO
        # @param quantity to reduce stock by
        # @param the products id
        public static function reduceStock($app_db_connection, $quantity, $product_id) {
            $update = $app_db_connection->prepare('UPDATE product SET product_stock = product_stock - :quantity WHERE product_stock - :quantity >= 0 AND product_id = :product_id');
            $update->bindParam(':quantity',     $quantity);
            $update->bindParam(':product_id',   $product_id);
            $update->execute();
        }

    }

?>