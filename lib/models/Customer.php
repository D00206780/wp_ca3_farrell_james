<?php

    class Customer {

        private $customer_id;
        private $first_name;
        private $last_name;
        private $gender;
        private $phone_number;
        private $email_address;
        private $eir_code;
        private $address_line_1;
        private $address_line_2;
        private $town_city;
        private $county;
        private $country;

        public function __construct($customer_id, $first_name, $last_name, $gender, $phone_number, $email_address, $eir_code, $address_line_1, $address_line_2, $town_city, $county, $country) {
            $this->customer_id = $customer_id;
            $this->first_name = $first_name;
            $this->last_name = $last_name;
            $this->gender = $gender;
            $this->phone_number = $phone_number;
            $this->email_address = $email_address;
            $this->eir_code = $eir_code;
            $this->address_line_1 = $address_line_1;
            $this->address_line_2 = $address_line_2;
            $this->town_city = $town_city;
            $this->county = $county;
            $this->country = $country;
        }

        # Retreive data about this customer
        # @param member variable name
        # return member variable value
        public function get($column_name) {
            return $this->$column_name;
        }

        # Create a new row in the customer table
        # @param database PDO
        # @param array of customer details
        public static function create($app_db_connection, $customer) {
            $insert = $app_db_connection->prepare('INSERT INTO customer (first_name, last_name, gender, phone_number, email_address, eir_code, address_line_1, address_line_2, town_city, county, country) VALUES (:first_name, :last_name, :gender, :phone_number, :email_address, :eir_code, :address_line_1, :address_line_2, :town_city, :county, :country);');
            $insert->bindParam(':first_name',       $customer['first_name']);
            $insert->bindParam(':last_name',        $customer['last_name']);
            $insert->bindParam(':gender',           $customer['gender']);
            $insert->bindParam(':phone_number',     $customer['phone_number']);
            $insert->bindParam(':email_address',    $customer['email_address']);
            $insert->bindParam(':eir_code',         $customer['eir_code']);
            $insert->bindParam(':address_line_1',   $customer['address_line_1']);
            $insert->bindParam(':address_line_2',   $customer['address_line_2']);
            $insert->bindParam(':town_city',        $customer['town_city']);
            $insert->bindParam(':county',           $customer['county']);
            $insert->bindParam(':country',          $customer['country']);
            $insert->execute();
        }

        # Update a row in the customer table
        # @param database PDO
        # @param array of customer details
        # @param the customers id
        public static function update($app_db_connection, $customer, $customer_id) {
            $update = $app_db_connection->prepare('UPDATE customer SET first_name = :first_name, last_name = :last_name, gender = :gender, phone_number = :phone_number, email_address = :email_address, eir_code = :eir_code, address_line_1 = :address_line_1, address_line_2 = :address_line_2, town_city = :town_city, county = :county, country = :country WHERE customer_id = :customer_id;');
            $update->bindParam(':customer_id',      $customer_id);
            $update->bindParam(':first_name',       $customer['first_name']);
            $update->bindParam(':last_name',        $customer['last_name']);
            $update->bindParam(':gender',           $customer['gender']);
            $update->bindParam(':phone_number',     $customer['phone_number']);
            $update->bindParam(':email_address',    $customer['email_address']);
            $update->bindParam(':eir_code',         $customer['eir_code']);
            $update->bindParam(':address_line_1',   $customer['address_line_1']);
            $update->bindParam(':address_line_2',   $customer['address_line_2']);
            $update->bindParam(':town_city',        $customer['town_city']);
            $update->bindParam(':county',           $customer['county']);
            $update->bindParam(':country',          $customer['country']);
            $update->execute();
        }

        # Delete a row from the customer table
        # @param database PDO
        # @param the customers id
        public static function delete($app_db_connection, $customer_id) {
            $delete = $app_db_connection->prepare('DELETE FROM customer WHERE customer_id = :customer_id');
            $delete->bindParam(':customer_id',      $customer_id);
            $delete->execute();
        }

        # Display information about a single customer
        # @param database PDO
        # @param the customers id
        # return array of customer details
        public static function displayCustomer($app_db_connection, $customer_id) {
            $select = $app_db_connection->prepare('SELECT * FROM customer WHERE customer_id = :customer_id LIMIT 1');
            $select->bindParam(':customer_id', $customer_id);
            $select->execute();
            $customer = $select->fetch();

            $customer = new Customer($customer['customer_id'], $customer['first_name'], $customer['last_name'], $customer['gender'], $customer['phone_number'], $customer['email_address'], $customer['eir_code'], $customer['address_line_1'], $customer['address_line_2'], $customer['town_city'], $customer['county'], $customer['country']);
            
            return [
                'customer_id'    => $customer->get('customer_id')    ?? '',
                'first_name'     => $customer->get('first_name')     ?? '',
                'last_name'      => $customer->get('last_name')      ?? '',
                'gender'         => $customer->get('gender')         ?? '',
                'phone_number'   => $customer->get('phone_number')   ?? '',
                'email_address'  => $customer->get('email_address')  ?? '',
                'eir_code'       => $customer->get('eir_code')       ?? '',
                'address_line_1' => $customer->get('address_line_1') ?? '',
                'address_line_2' => $customer->get('address_line_2') ?? '',
                'town_city'      => $customer->get('town_city')      ?? '',
                'county'         => $customer->get('county')         ?? '',
                'country'        => $customer->get('country')        ?? '',
            ];
        }

        # Display all customers in table
        # @param database PDO
        # return an array of customer objects
        public static function displayAll($app_db_connection) {
            $select = $app_db_connection->query('SELECT * FROM customer');
            $items  = $select->fetchAll();

            $customers = [];

            foreach ($items as $item) {
                $customer = new Customer($item['customer_id'], $item['first_name'], $item['last_name'], $item['gender'], $item['phone_number'], $item['email_address'], $item['eir_code'], $item['address_line_1'], $item['address_line_2'], $item['town_city'], $item['county'], $item['country']);
                array_push($customers, $customer);
            }

            return $customers;
        }

        # Check if a value exists in the table
        # @param database PDO
        # @param name of the column to check
        # @param name of the value to check
        # return true if the value exists
        public static function checkIfExists($app_db_connection, $column_name, $column_value) {
            $select = $app_db_connection->prepare("SELECT * FROM customer WHERE $column_name = :column_value LIMIT 1");
            $select->bindParam(':column_value',     $column_value);
            $select->execute();
            return $select->rowCount() !== 0;
        }

    }

?>