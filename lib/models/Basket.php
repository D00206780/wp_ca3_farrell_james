<?php 

    include('BasketItem.php');

    class Basket {

        # Display all items in the basket
        # @param database PDO
        # @param the customers id
        # return array of basket items
        public static function displayAll($app_db_connection, $customer_id) {
            $select = $app_db_connection->prepare('SELECT c.customer_id, p.product_id, p.product_name, p.product_price, COUNT(*) AS quantity, COUNT(*) * product_price AS total_cost FROM product p, basket b, customer c WHERE b.customer_id = c.customer_id AND p.product_id = b.product_id AND c.customer_id = :customer_id GROUP BY p.product_id');
            $select->bindParam(':customer_id', $customer_id);
            $select->execute();
            $items  = $select->fetchAll();

            $basket_items = [];

            foreach ($items as $item) {
                $basket_item = new BasketItem($item['customer_id'], $item['product_id'], $item['product_name'], $item['product_price'], $item['quantity'], $item['total_cost']);
                array_push($basket_items, $basket_item);
            }
            
            return $basket_items;
        }

        # Add x amount of a product to the basket
        # @param database PDO
        # @param quantity of product to add
        # @oaram the customers id
        # @param the products id
        public static function add($app_db_connection, $quantity, $customer_id, $product_id) {
            for ($i = 0; $i < $quantity; $i++) {
                $insert = $app_db_connection->prepare('INSERT INTO basket (customer_id, product_id) VALUES (:customer_id, :product_id)');
                $insert->bindParam(':customer_id',  $customer_id);
                $insert->bindParam(':product_id',   $product_id);
                $insert->execute();
            }
        }

        # Remove all occurences of a product from the basket
        # @param database PDO
        # @oaram the customers id
        # @param the products id
        public static function remove($app_db_connection, $customer_id, $product_id) {
            $delete = $app_db_connection->prepare('DELETE FROM basket WHERE customer_id = :customer_id AND product_id = :product_id');
            $delete->bindParam(':customer_id', $customer_id);
            $delete->bindParam(':product_id', $product_id);
            $delete->execute();
        }

    }

?>