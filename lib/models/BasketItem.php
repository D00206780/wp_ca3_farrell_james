<?php 

    class BasketItem {

        private $product_id;
        private $customer_id;
        private $product_name;
        private $product_price;
        private $quantity;
        private $total_cost;

        public function __construct($customer_id, $product_id, $product_name, $product_price, $quantity, $total_cost) {
            $this->customer_id   = $customer_id;
            $this->product_id    = $product_id;
            $this->product_name  = $product_name;
            $this->product_price = $product_price;
            $this->quantity      = $quantity;
            $this->total_cost    = $total_cost;
        }

        # Retreive data about this basket item
        # @param member variable name
        # return member variable value
        public function get($column_name) {
            return $this->$column_name;
        }

    }

?>