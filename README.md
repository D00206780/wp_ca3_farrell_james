# WireMart - Online Shopping!
______________________________________________________
WireMart models a very simple online shop.
______________________________________________________
### On this page:

[Functionality](#functionality)  
[Database](#database)  
[Setup](#setup)  
[References](#references)  
[Todo](#todo)  
[Comments](#comments)  
______________________________________________________

#### Functionality

* Customers may add or remove products from their basket. 
* Customers may purchase any item in their basket.
* Customers may not add more items thanthey 
______________________________________________________
### Database

1. Entities    
    * There are two main entities
        * Customers
        * Products  

    * There is one association entity  
        * Basket

2. Relationships  
    * Base Relationship
        * Customers may purchase many products
        * A product may be purchased by many customers
    
    * Association Relationship
        * A customer may own one basket
        * A basket may be owned by many customers
        * A product may be in many baskets
        * A basket may have many products

3. Schema

    ![Database Schema](assets\images\database_schema.png)

______________________________________________________
### Setup

1. Download project

    * Clone the project into your directory of choice:  
    `git clone https://D00206780@bitbucket.org/D00206780/wp_ca3_farrell_james.git`

2. Create Database

    * Create a PhpMyAdmin database 
    * Import the included [MySQL file](assets\sql\wp_ca3_farrell_james.sql) to the database  

3. Configure Project

    * Create a config.php file at the project root
    * Copy the contents of [config.php.sample](config.php.sample) into the config file
    * Edit config.php (see [file](config.php.sample) for instructions)
    * Edit [.htacces](.htacces) (see [file](.htacces) for instructions)

______________________________________________________
### References

1. Code Base  

    * The code base for this application was pulled from a GitLab repository, created by Shane Gavin.  
    * Shane discussed the Rapid MVC framework with us, as we developed it in class.
        * [GitLab Repo](https://gitlab.comp.dkit.ie/gavins/rapid-starter-project)
    

2. Database Design  

    * When designing my database, I decided to research limits and default field values.     
    * This would allow me to accurately reflect a real world scenario.      
    * A list of the references that I used have been added below.    

    * Resources used to help define database structure:   
        * [Shop Schema](https://stackoverflow.com/questions/35612778/database-schema-for-an-online-shop)

    * Resources used to help identify upper limits and field types:  
        * [First Name](https://stackoverflow.com/questions/20958/list-of-standard-lengths-for-database-fields)
        * [Last Name](https://stackoverflow.com/questions/20958/list-of-standard-lengths-for-database-fields)
        * [Gender](https://stackoverflow.com/questions/4175878/storing-sex-gender-in-database)
        * [Phone Number](https://stackoverflow.com/questions/723587/whats-the-longest-possible-worldwide-phone-number-i-should-consider-in-sql-varc)
        * [Email Address](https://stackoverflow.com/questions/20958/list-of-standard-lengths-for-database-fields)
        * [Post Code](https://stackoverflow.com/questions/325041/i-need-to-store-postal-codes-in-a-database-how-big-should-the-column-be)
        * [Street Address](https://stackoverflow.com/questions/20958/list-of-standard-lengths-for-database-fields)
        * [Town](https://www.worldatlas.com/articles/the-10-longest-place-names-in-the-world.html)
        * [City](https://stackoverflow.com/questions/20958/list-of-standard-lengths-for-database-fields)
        * [Country](http://itre.cis.upenn.edu/~myl/languagelog/archives/004109.html)  

3. Recycled Assets

    * The following assets have been extracted from previous projects:
        * [FormUtils](lib\utils\FormUtils.php) - this file was originally created by Shane Gavin
        * [Errors](lib\errors\Errors.php)
        * [Database](lib\utils\Database.php)

    * It should be noted that all files have changed substantially to suit the design of the current website. 

4. Acknowledgements

    * Cameron Scholes helped me to setup values in my config.php file
    * Cameron Scholes helped me to test my site
    * Nathan Hill helped me to test my site
    * Christian Rafferty helped me to test my site
    * Aaron Richardson helped me to test my site
    * Aisling Cunnigham helped me to test my site

5. Stack Overflow

    * Stack Overflow helped me overcome some problems that I faced.  
    * References have been added throughout.

______________________________________________________

###  Todo
* Add Northern Ireland support
* Add refill select fields support

______________________________________________________

### Comments

1. General
    * The user is considered to be an admin until they view a customers basket, at which point there are considered to be acting as that customer.

2. Support
    * Currently, this website is designed to only handle customers living within the Republic of Ireland. 
    * Currently, there is not support for re-filling select fields with data extracted from the database.

3. Reflections
    * I'm currently not satisfied with how the AddProduct controller handles errors. 
    * I'm also not satisfied with how the Customer and Product model returns a single customer/product.

______________________________________________________